#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
using namespace::std;
class Worktime
{
public:
	Worktime(void);
	virtual int TimePayment(int _worktime, int _hourpayment) = 0;
	~Worktime(void);
};


class Project
{
public:
	Project(void);
	virtual int ProjectPayment(int _projectbudget, double _projectwork) = 0;
	~Project(void);
};


class Heading
{
public:
	Heading(void);
	virtual int HeadingPayment(int _headingbase, int _teamsize) = 0;
	~Heading(void);
};