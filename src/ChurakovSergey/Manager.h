#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include "Employee.h"
class Manager: public Employee, public Project
{
protected:
	int ProjectBudget;
	double ProjectWork;
	string ProjectName;
public:
	int GetPBudget()
	{
		return ProjectBudget;
	}
	double GetPWork()
	{
		return ProjectWork;
	}
	void SetPBudget(int _pbudget)
	{
		ProjectBudget = _pbudget;
	}
	void SetPWork(double _pwork)
	{
		ProjectWork = _pwork;
	}
	string GetPName()
	{
		return ProjectName;
	}
	void SetPName(string _pname)
	{
		ProjectName = _pname;
	}
	Manager();
	Manager(int _id, string _name, int _worktime, string _pname, int _projectwork, int _projectBudget);
	~Manager();
	int ProjectPayment(int _projectBudget, double _projectwork);
	int GetPayment();
	void Print(ostream& stream);
};


class ProjectManager : public Manager, public Heading
{
protected:
	Manager* Team ;
	int TeamSize;
	int HeadingBase = 4500;
public:
	int GetTeamSize()
	{
		return TeamSize;
	}
	void SetTeamSize(int _tsize)
	{
		TeamSize = _tsize;
	}
	ProjectManager();
	ProjectManager(int _id, string _name, int _worktime, string _pname, int _projectwork, int _projectBudget, int TeamSize);
	~ProjectManager();
	void SetTeam(Manager _team[]);
	int HeadingPayment(int _headingbase, int _teamsize);
	int GetPayment();
	void Print(ostream& stream);
};

class SeniorManager: public ProjectManager
{
protected:
	int ProjectNumber;
public:
	int GetProjectNumber()
	{
		return ProjectNumber;
	}
	void SetPNumber(int _pnumber)
	{
		ProjectNumber = _pnumber;
	}
	void IncreasePNumber()
	{
		ProjectNumber++;
	}
	SeniorManager();
	~SeniorManager();
	SeniorManager(int _id, string _name, int _worktime, string _pname, int _projectwork, int _projectBudget,int _projectnumber, int _teamsize);
	void Print(ostream& stream);
};