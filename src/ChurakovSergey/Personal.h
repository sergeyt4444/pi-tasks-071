#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include "Employee.h"
using namespace::std;
class Personal: public Employee, public Worktime
{
protected:
	int HourPayment;
public:
	int GetHPayment()
	{
		return HourPayment;
	}
	void SetHPayment(int _hpayment)
	{
		HourPayment = _hpayment;
	}
	Personal(void);
	Personal(int _id, string _name, int _worktime, int _HourPayment);
	~Personal(void);
	virtual int TimePayment(int _worktime, int _hourpayment);
	virtual int GetPayment();
};

class Cleaner : public Personal
{
public:
	Cleaner(void);
	Cleaner(int _id, string _name, int _worktime,int _HourPayment);
	~Cleaner(void);
	virtual int TimePayment(int _worktime, int _hourpayment);
	virtual int GetPayment();
	virtual void Print(ostream& stream);
};

class Driver : public Personal
{
public:
	Driver(void);
	Driver(int _id, string _name, int _worktime,int _HourPayment);
	~Driver(void);
	virtual int TimePayment(int _worktime, int _hourpayment);
	virtual int GetPayment();
	void Print(ostream& stream);
};

