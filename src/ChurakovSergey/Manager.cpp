#include "Manager.h"



Manager::Manager(): Employee()
{
	ProjectWork = .0;
	ProjectBudget = 0;
}

Manager::Manager(int _id, string _name, int _worktime, string _pname, int _projectwork, int _projectbudget)
{
	id = _id;
	name = _name;
	worktime = _worktime;
	ProjectWork = _projectwork;
	ProjectBudget = _projectbudget;
	ProjectName = _pname;
}


Manager::~Manager()
{
}

int Manager::ProjectPayment(int _projectBudget, double _projectwork)
{
	return (int)(_projectwork*_projectBudget);
}

int Manager::GetPayment()
{
	payment = ProjectPayment(ProjectBudget, ProjectWork);
	return payment;
}

void Manager::Print(ostream & stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: Manager" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Project title: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << endl;
}



ProjectManager::ProjectManager(): Manager()
{
	TeamSize = 0;
	Team = new Manager[20];
}

ProjectManager::ProjectManager(int _id, string _name, int _worktime, string _pname, int _projectwork, int _projectbudget, int _teamsize)
{
	Manager(_id, _name, _worktime,_pname, _projectwork, _projectbudget);
	TeamSize = _teamsize;
}


ProjectManager::~ProjectManager()
{
}

void ProjectManager::SetTeam(Manager _team[])
{
	for (int i = 0; i < TeamSize; i++)
		Team[i] = _team[i];
}

int ProjectManager::HeadingPayment(int _headingbase, int _teamsize)
{
	return TeamSize*HeadingBase;
}

int ProjectManager::GetPayment()
{
	if (ProjectWork <= 1)
	{

		payment = ProjectPayment(ProjectWork, ProjectBudget)+HeadingPayment(HeadingBase, TeamSize);
		return payment;
	}
	else
	{
		cout << "Error";
		return 0;
	}
}

void ProjectManager::Print(ostream & stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: Project Manager" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Project title: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << "Size of team: " << GetTeamSize() << endl;
	stream << endl;
}

SeniorManager::SeniorManager():ProjectManager()
{
	ProjectNumber = 0;
}

SeniorManager::~SeniorManager()
{
}

SeniorManager::SeniorManager(int _id, string _name, int _worktime, string _pname, int  _projectwork, int  _projectbudget, int _projectnumber, int _teamsize):ProjectManager(_id, _name, _worktime, _pname, _projectwork, _projectbudget, _teamsize)
{
	ProjectNumber = _projectnumber;
}




void SeniorManager::Print(ostream & stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: SeniorManager" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Number of projects: " << GetProjectNumber() << endl;
	stream << "Project titles: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << "Size of team: " << GetTeamSize() << endl;
	stream << endl;
}
