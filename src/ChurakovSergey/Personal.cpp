#include "Personal.h"


Personal::Personal(void): Employee()
{
	HourPayment = 0;
}

Personal::Personal(int _id, string _name, int _worktime,int _HourPayment)
{
	id = _id;
	name = _name;
	worktime = _worktime;
	HourPayment = _HourPayment;
}


Personal::~Personal(void)
{
}

int Personal::TimePayment(int _worktime, int _hourpayment)
{
	return _hourpayment*_worktime * 20;
}

int Personal::GetPayment()
{
	payment = TimePayment(worktime, HourPayment);
	return payment;
}
Cleaner::Cleaner(void): Personal()
{
}

Cleaner::Cleaner(int _id, string _name, int _worktime, int _HourPayment):Personal(_id,  _name,  _worktime,  _HourPayment)
{
}


Cleaner::~Cleaner(void)
{
}
int Cleaner::TimePayment(int _worktime, int _hourpayment)
{
	return _hourpayment*_worktime * 20;
}

int Cleaner::GetPayment()
{
	payment = TimePayment( worktime,  HourPayment);
	return payment;
}

void Cleaner::Print(ostream& stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: Cleaner" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Money per hour: " << GetHPayment() << endl;
	stream << endl;
}


Driver::Driver(void): Personal()
{

}

Driver::Driver(int _id, string _name, int _worktime, int _HourPayment):	Personal(_id, _name, _worktime, _HourPayment)
{

}


Driver::~Driver(void)
{
}
int Driver::TimePayment(int _worktime, int _hourpayment)
{
	return _hourpayment*_worktime * 20;
}

int Driver::GetPayment()
{
	payment = TimePayment( worktime,  HourPayment);
	return payment;
}

void Driver::Print(ostream & stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: Driver" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Money per hour: " << GetHPayment() << endl;
	stream << endl;
}
