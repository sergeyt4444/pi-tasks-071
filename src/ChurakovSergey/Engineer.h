#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include "Employee.h"
using namespace::std;
class Engineer: public Employee,public Worktime, public Project
{
protected:
	int HourPayment;
	int ProjectBudget;
	double ProjectWork;
	string ProjectName;
public:
	int GetHPayment()
	{
		return HourPayment;
	}
	int GetPBudget()
	{
		return ProjectBudget;
	}
	double GetPWork()
	{
		return ProjectWork;
	}
	void SetHPayment(int _hpayment)
	{
		HourPayment = _hpayment;
	}
	void SetPBudget(int _pbudget)
	{
		ProjectBudget = _pbudget;
	}
	void SetPWork(double _pwork)
	{
		ProjectWork = _pwork;
	}
	string GetPName()
	{
		return ProjectName;
	}
	void SetPName(string _pname)
	{
		ProjectName = _pname;
	}
	Engineer();
	Engineer(int _id, string _name, int _worktime, int _hourpayment, string _pname, int _projectwork, int _projectBudget);
	~Engineer();
	 int TimePayment(int _worktime, int _hourpayment);
	 int ProjectPayment( int _projectBudget,double _projectwork);
	int GetPayment();
	void Print(ostream& stream);
};


class Tester : public Engineer
{
public:
	Tester();
	Tester(int _id, string _name, int _worktime, int _hourpayment, string _pname, int _projectwork, int _projectBudget);
	~Tester();
	void Print(ostream& stream);
};

class Programmer : public Engineer
{
public:
	Programmer();
	Programmer(int _id, string _name, int _worktime,int _hourpayment, string _pname, int _projectwork, int _projectBudget);
	~Programmer();
	void Print(ostream& stream);
};

class Teamleader : public Programmer, public Heading
{
protected:
	Programmer* Team ;
	int TeamSize;
	int HeadingBase;
public:
	int GetTeamSize()
	{
		return TeamSize;
	}
	void SetTeamSize(int _tsize)
	{
		TeamSize = _tsize;
	}
	Teamleader();
	Teamleader(int _id, string _name, int _worktime, int _hourpayment, string _pname, int _projectwork, int _projectBudget, int _teamsize);
	~Teamleader();
	void SetTeam(Programmer _team[]);
	void Print(ostream& stream);

	 int HeadingPayment(int _headingbase, int _teamsize);
	 int GetPayment();
};
