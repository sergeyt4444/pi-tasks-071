#include "Engineer.h"



Engineer::Engineer(): Employee()
{
	ProjectWork = .0;
	ProjectBudget = 0;
	HourPayment = 0;
}

Engineer::Engineer(int _id, string _name, int _worktime, int _hourpayment, string _pname, int _projectwork, int _projectbudget) : HourPayment(_hourpayment), ProjectWork(_projectwork), ProjectBudget(_projectbudget)
{
	id = _id;
	name = _name;
	worktime = _worktime;
	ProjectName = _pname;
}


Engineer::~Engineer()
{
}

int Engineer::TimePayment(int _worktime, int _hourpayment)
{
	return _hourpayment*_worktime * 20;
}

int Engineer::ProjectPayment(int _projectBudget,double _projectwork)
{
	return (int)(ProjectWork*ProjectBudget);
}

int Engineer::GetPayment()
{
	if (ProjectWork <= 1)
	{
		int _worktime = TimePayment(worktime, HourPayment);
		int _project = ProjectPayment(ProjectBudget, ProjectWork);
		payment = _worktime + _project;
		return payment;
	}
	else
	{
		cout << "Error";
		return 0;
	}

}

void Engineer::Print(ostream & stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: Engineer" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Money per hour: " << GetHPayment() << endl;
	stream << "Project title: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << endl;
}

Tester::Tester(): Engineer()
{
}

Tester::Tester(int _id, string _name, int _worktime,  int _hourpayment,string _pname, int _projectwork, int _projectBudget)
{
	Engineer(_id, _name, _worktime,_hourpayment, _pname, _projectwork, _projectBudget);
}


Tester::~Tester()
{
}

void Tester::Print(ostream & stream)
{
	stream << "ID: " <<  GetID()<< endl;
	stream << "Job: Tester" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Money per hour: " << GetHPayment() << endl;
	stream << "Project title: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << endl;
}


Programmer::Programmer():Engineer()
{
}

Programmer::Programmer(int _id, string _name, int _worktime,  int _hourpayment,string _pname, int _projectwork, int _projectBudget)
{
	Engineer(_id, _name, _worktime, _hourpayment,_pname, _projectwork, _projectBudget);
}


Programmer::~Programmer()
{
}

void Programmer::Print(ostream & stream)
{
	stream << "ID: " << GetID() << endl;
	stream << "Job: Programmer" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Money per hour: " << GetHPayment() << endl;
	stream << "Project title: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << endl;
}

Teamleader::Teamleader():Programmer()
{
	TeamSize = 0;
	Team = new Programmer[5];
	HeadingBase = 5000;
}

Teamleader::Teamleader(int _id, string _name, int _worktime, int _hourpayment, string _pname, int _projectwork, int _projectBudget, int _teamsize)
{
	Engineer(_id, _name, _worktime, _hourpayment,_pname, _projectwork, _projectBudget);
	TeamSize = _teamsize;
	HeadingBase = 5000;
}


Teamleader::~Teamleader()
{
	delete[] Team;
	Team = nullptr;
}

void Teamleader::SetTeam(Programmer _team[])
{
	for (int i = 0; i < TeamSize; i++)
		Team[i] = _team[i];
}

void Teamleader::Print(ostream & stream)
{
	stream << "ID: "<<GetID() << endl;
	stream << "Job: TeamLeader" << endl;
	stream << "Fio: " << GetName() << endl;
	stream << "Worktime: " << GetWorktime() << endl;
	stream << "Salary: " << GetRawPayment() << endl;
	stream << "Money per hour: " << GetHPayment() << endl;
	stream << "Project title: " << GetPName() << endl;
	stream << "Project Budget:" << GetPBudget() << endl;
	stream << "Activity: " << GetPWork() << endl;
	stream << "Size of team: " << GetTeamSize() << endl;
	stream << endl;
}



int Teamleader::HeadingPayment(int _headingbase, int _teamsize)
{
	return _teamsize*_headingbase;
}

int Teamleader::GetPayment()
{
	if (ProjectWork <= 1)
	{
		int _worktime = TimePayment(worktime, HourPayment);
		int _project = ProjectPayment(ProjectWork, ProjectBudget);
		int _heading = HeadingPayment(HeadingBase, TeamSize);
		
		payment = _worktime + _project+_heading;
		return payment;
	}
	else
	{
		cout << "Error";
		return 0;
	}

}

