#pragma once
#include <string>
#include <iostream>
#include <cstdlib>
#include "Worktime.h"
using namespace::std;
class Employee
{

protected:

	int id;
	string name;
	int payment;
	int worktime;

public:
	int GetID()
	{
		return id;
	}
	string GetName()
	{
		return name;
	}
	int GetRawPayment()
	{
		return payment;
	}

	int GetWorktime()
	{
		return worktime;
	}
	void SetID(int _id)
	{
		id = _id;
	}
	void SetWorktime(int _worktime)
	{
		worktime = _worktime;
	}
	void SetName(string _name)
	{
		name = _name;
	}
	Employee(void);
	Employee(int id, string name, int worktime);
	~Employee(void);
	virtual int GetPayment()=0;
	virtual void Print(ostream& stream) = 0;
};

