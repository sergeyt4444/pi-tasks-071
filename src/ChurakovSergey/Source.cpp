
#include <string>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include "Employee.h"
#include "Worktime.h"
#include "Manager.h"
#include "Personal.h"
#include "Engineer.h"
using namespace::std;

int main()
{
	Employee* Staff[50];
	int i = 0;
	ifstream File("TextFile3.txt");
	if (!File.is_open())
	 {
		cout << "Error" << endl;
		exit(EXIT_FAILURE);
		}
	string strID, strName, strPosition, strWorkTime,  temp;
	while (!File.eof())
	{
		getline(File, strID);
		getline(File, strName);
		getline(File, strPosition);
		getline(File, strWorkTime);
		getline(File, temp);
		if (strPosition == "Cleaner")
		{
			Cleaner* cl = new Cleaner();
			string strHPayment;
			getline(File, strHPayment);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetHPayment(atoi(strHPayment.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}
		if (strPosition == "Driver")
		{
			Driver* cl = new Driver();
			string strHPayment;
			getline(File, strHPayment);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetHPayment(atoi(strHPayment.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}
		if (strPosition == "Programmer")
		{
			Programmer* cl = new Programmer();
			string strHPayment, strPBudget, strPWork;
			string strPName;
			getline(File, strPName);
			getline(File, strPWork);
			getline(File, strHPayment);
			getline(File, strPBudget);
			cl->SetPName(strPName);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetPWork(atof(strPWork.c_str()));
			cl->SetHPayment(atoi(strHPayment.c_str()));
			cl->SetPBudget(atoi(strPBudget.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}
		if (strPosition == "Tester")
		{
			Tester* cl = new Tester();
			string strHPayment, strPBudget, strPWork;
			string strPName;
			getline(File, strPName);
			getline(File, strPWork);
			getline(File, strHPayment);
			getline(File, strPBudget);
			cl->SetPName(strPName);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetPWork(atof(strPWork.c_str()));
			cl->SetHPayment(atoi(strHPayment.c_str()));
			cl->SetPBudget(atoi(strPBudget.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}

		if (strPosition == "TeamLeader")
		{
			Teamleader* cl = new Teamleader();
			string strHPayment, strPBudget, strPWork, strTSize;
			string strPName;
			getline(File, strPName);
			getline(File, strPWork);
			getline(File, strHPayment);
			getline(File, strPBudget);
			getline(File, strTSize);
			cl->SetPName(strPName);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetPWork(atof(strPWork.c_str()));
			cl->SetHPayment(atoi(strHPayment.c_str()));
			cl->SetPBudget(atoi(strPBudget.c_str()));
			cl->SetTeamSize(atoi(strTSize.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}

		if (strPosition == "Manager")
		{
			Manager* cl = new Manager();
			string  strPBudget, strPWork;
			string strPName;
			getline(File, strPName);
			getline(File, strPWork);
			getline(File, strPBudget);
			cl->SetPName(strPName);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetPWork(atof(strPWork.c_str()));
			cl->SetPBudget(atoi(strPBudget.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}
		if (strPosition == "ProjectManager")
		{
			ProjectManager* cl = new ProjectManager();
			string  strPBudget, strPWork, strTSize;
			string strPName;
			getline(File, strPName);
			getline(File, strPWork);
			getline(File, strPBudget);
			getline(File, strTSize);
			cl->SetPName(strPName);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetPWork(atof(strPWork.c_str()));
			cl->SetPBudget(atoi(strPBudget.c_str()));
			cl->SetTeamSize(atoi(strTSize.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}
		if (strPosition == "SeniorManager")
		{
			SeniorManager* cl = new SeniorManager();
			string  strPBudget, strPWork, strTSize, strPNum, strPName ;

			getline(File, strPNum);
			getline(File, strPName);
			getline(File, strPWork);
			getline(File, strPBudget);
			getline(File, strTSize);
			cl->SetID(atoi(strID.c_str()));
			cl->SetName(strName);
			cl->SetWorktime(atoi(strWorkTime.c_str()));
			cl->SetPName(strPName);
			cl->SetPWork(atof(strPWork.c_str()));
			cl->SetPBudget(atoi(strPBudget.c_str()));
			cl->SetTeamSize(atoi(strTSize.c_str()));
			getline(File, temp);
			Staff[i] = cl;
			Staff[i++]->GetPayment();

		}

	}
	int NumofEmp = i;
		File.close();
			ofstream data_out("TextFile2.txt");
		
			for (i = 0; i < NumofEmp; i++)
			{
				 if (!data_out.is_open())
				 {
				cout << "Error" << endl;
				exit(EXIT_FAILURE);
				}
			Staff[i]->Print(data_out);


				 }
			cout << "Calculations were successfull" << endl;
	data_out.close();
	return 0;
}
